import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  title: string = "Add User";

  userName: string = "test";

  @Output() addUser = new EventEmitter<string>();


  constructor() { }

  ngOnInit(): void {
  }

  onSubmitRegister (input: HTMLInputElement){
    this.addUser.emit(input.value);
  }


}
